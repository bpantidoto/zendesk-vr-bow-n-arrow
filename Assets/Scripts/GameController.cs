﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class GameController : MonoBehaviour
{
    [Header("--Result info--")]
    public GameObject result;
    public Text timeResultText;
    public Text rightsResultText;
    public Text wrongsResultText;
    public Text pointsResultText;
    public Text bonusPointsText;
    public int bonusPoints;
    public int rightAnswers;
    public int wrongAnswers;

    [Header("--Scenes Info--")]
    public string[] sceneName;
    public string currentScene;

	[Header("--Intro Info--")]
	public float timeBeforeGo;
	public bool goForLoading;
	public bool GoForMainScreen;
    public Text user;

    [Header("--TutorialIntro Info.--")]
    public GameObject introGameObject;
    private IntroController introController;
    public float timeAfterIntro;
    public float timeAfterPreGame;

    [Header("--Tutorial Info--")]
    public int numberOfSteps;
    public int currentStep;
    public float timeBetweenTutorialStep;
    private TutorialController tutorialController;
    public float timeAfterTutorial;
    
    [Header("--Quiz Info--")]
    public int numberOfQuestions;
    public int numberOfAnswers;
    private QuestionModel questionInfo;
    private HUDController hudController;
    private TargetController targetController;
    private int currentQuestion;
    public float timeAfterQuiz;
    public float limitTimeQuizQuestion;
    public float timeStartClock;

    [Header("--QA Controller--")]
    public bool canShoot; //TRUE when the player can shoot (NEEDS TO BE PUBLIC)     
    public int pointsByAnswer;
    public float timeBetweenQuestions;
    public int maxTimeInMilliseconds;
    public int timeMultiplier;

    [Header("--Flow Controller--")]
    public bool introPart;
    public bool introStarted;
    public bool lastIntroPart;
    public bool tutorialPart;
    public bool tutorialStarted;
    public bool quizPart;
    public bool quizStarted;
    public bool bonusPart;
    public bool bonusStarted;
    public bool endPart;
    public float timeBeforeIntro;

    [Header("--End Info--")]
    public Image endImage;
    public GameObject sun;

    private void Start()
    {
        if(SceneController.instance != null)
        {
            //Everytime a scene starts, make it fadeIn.
            SceneController.instance.OpenScene();

            //Save the name of the current scene
            currentScene = SceneController.instance.GetCurrentSceneName();

            //If is in the loadingScene, show name
            if (currentScene == sceneName[2])
            {
                //NFC
                //user.text = PlayerPrefs.GetString("name");
                //user.text = "Jogador";
            }

            //If is in the second scene - Quiz Scene
            if (currentScene == sceneName[1]){
                SetupQuizScene();

                introPart = true;
                introStarted = true;
                StartCoroutine(WaitToStartIntro());
            }
        }
    }
    private IEnumerator WaitToStartIntro()
    {
        yield return new WaitForSeconds(timeBeforeIntro);
        introPart = true;
        introStarted = false;
    }
    private void SetupQuizScene()
    {
        //Init the QuestionView (To get information about the questions)
        questionInfo = gameObject.GetComponent<QuestionModel>();

        //Generate the Questions that will be used in this game
        questionInfo.GenerateRandomQuestionArray(numberOfQuestions);

        //Init the HUDController 
        hudController = gameObject.GetComponent<HUDController>();

        //Init the TargetController
        targetController = gameObject.GetComponent<TargetController>();

        //Init the tutorial controller
        tutorialController = gameObject.GetComponent<TutorialController>();

        //--HUD--
        //Show user name - NFC
        //hudController.SetPlayerName(PlayerPrefs.GetString("name"));

        //zero points
        hudController.SetPoints(0);

        //reset time
        hudController.ResetTime();
    }
    private void FixedUpdate()
    {
        if (currentScene == sceneName[1])
        {
            //0 - Intro
            if (introPart && !introStarted)
            {
                introStarted = true;
                StartIntro();
            }

            //1 - tutorial
            if (tutorialPart && !tutorialStarted)
            {
                tutorialStarted = true;
                StartTutorial();
            }

            //2 - Quiz
            if (quizPart && !quizStarted)
            {
                quizStarted = true;
                StartQuiz();
            }

            //3 - bonus
            if (bonusPart && !bonusStarted)
            {
                bonusStarted = true;
                StartBonus();
            }
        }
    }

    #region Introduction
    private void StartIntro()
    {
        //Enable the intro gameobject
        introGameObject.SetActive(true);

        //IntroController
        introController = gameObject.GetComponent<IntroController>();

        //Show first step
        introController.ShowStepImage(introController.GetCurrentStep());
    }
    public void EndIntro()
    {
        StartCoroutine(WaitToStartTutorial());
    }
    private IEnumerator WaitToStartTutorial()
    {
        yield return new WaitForSeconds(timeAfterIntro);
        introPart = false;
        introStarted = false;
        tutorialPart = true;
        tutorialStarted = false;
    }
    public void EndPreGame()
    {
        StartCoroutine(WaitToStartQuiz());
    }
    private IEnumerator WaitToStartQuiz()
    {
        yield return new WaitForSeconds(timeAfterPreGame);
        tutorialPart = false;
        tutorialStarted = false;
        quizPart = true;
        quizStarted = false;
    }
    #endregion

    #region Tutorial
    private void StartTutorial()
    {
        if(tutorialController != null)
        {
            //Start in the first Step
            currentStep = tutorialController.GetStep();

            //Start first Step
            StartStep(currentStep);
        }
    }
    private void StartStep(int step)
    {
        //Show targets of the step
        StartCoroutine(WaitToShowTarget(currentStep));

        //Enable the player to shoot
        canShoot = true;
    }
    private IEnumerator WaitToShowTarget(int step)
    {
        float timeToShow = tutorialController.GetTimeBeforeShowTarget();
        yield return new WaitForSeconds(timeToShow);
        tutorialController.ShowTarget(step);
    }
    public void EndStep()
    {
        //unable the player to shoot
        canShoot = false;

        //Check if is the last step
        if (tutorialController.IsLastStep()){
            EndTutorialPart();
        }
        else{
            tutorialController.NextStep();
            tutorialController.HideAllOnTargets();
            currentStep = tutorialController.GetStep();
            StartCoroutine(WaitToStartNextTutorialStep());
        }
    }
    private IEnumerator WaitToStartNextTutorialStep()
    {
        yield return new WaitForSeconds(timeBetweenTutorialStep);
        StartStep(currentStep);
    }
    private void EndTutorialPart()
    {
        StartCoroutine(WaitToStartPreGame());
    }
    private IEnumerator WaitToStartPreGame()
    {
        yield return new WaitForSeconds(timeAfterTutorial);
        lastIntroPart = true;
        introController.ShowLastStep();
    }
    #endregion

    #region Question
    private void StartQuiz()
    {
        lastIntroPart = false;

        //Start in the first question
        currentQuestion = questionInfo.GetCurrentQuestion();
        //Start time count
        hudController.StartTimeCount();

        //Start the first question
        StartQuestion(currentQuestion);
    }
    private void StartQuestion(int question)
    {
        //--HUD--
        //Show the HUD
        hudController.ShowHUD();
        //Show question
        hudController.SetQuestionTitle(questionInfo.GetQuestionTitle(currentQuestion));
        //Set Answers
        hudController.SetAnswers(questionInfo.GetAllAnswers(currentQuestion));
        //Show the current question feedback
        hudController.SetNumQuestionFeedBack(currentQuestion + 1, numberOfQuestions);

        //--TARGETS--
        //Randomize the targets to show
        targetController.GenerateRandomTargetArray(numberOfAnswers);
        //Show the targets
        targetController.ShowRandomTargets();

        //--BOOLEANS--
        canShoot = true;

        //--TIME--
        if (question != 0){
            if(AudioController.instance != null)
            {            
                AudioController.instance.PlaybackgroundQuiz();
            }
        }
        questionInfo.StartCountingQuestionTime(Time.time, limitTimeQuizQuestion, timeStartClock);
        hudController.StartTimeCountQuestion();
    } 
    public IEnumerator WaitToShowTargets()
    {
        yield return new WaitForSeconds(1);
    }
    public void AnswerChoosed(int alternative)
    {
        questionInfo.cancelClock = true;

        //--TARGETS--
        //hide the targets that are visiable
        targetController.HideRandomTargets();

        //check if it was the right answer
        bool isRight = questionInfo.IsAnswerRight(currentQuestion, alternative);

        //--FEEDBACK--
        //visual
        //hudController.SetVisualFeedback(questionInfo.GetQuestionRightIndex(currentQuestion));
        hudController.SetVisualFeedBack(questionInfo.GetQuestionRightIndex(currentQuestion), alternative);
        //sound
        GiveAudioFeedback(isRight);
        
        if(AudioController.instance != null)
            AudioController.instance.ClockStop();

        //--POINTS--
        float questionTime = hudController.EndTimeCountQuestion();
        float bonusPoints = (28.0f - questionTime) * 296;

        if (isRight){
            hudController.AddPoints(pointsByAnswer);
            hudController.AddPoints((int)bonusPoints);
            rightAnswers++;
            
            TxtFileLoader.AddTextOnFile(Application.persistentDataPath + "/DB_Text.txt", questionInfo.GetQuestionTitle(currentQuestion) +"\n  Correto");
        }
        else{
            wrongAnswers++;
            TxtFileLoader.AddTextOnFile(Application.persistentDataPath + "/DB_Text.txt", questionInfo.GetQuestionTitle(currentQuestion) + "\n  Errado");
        }

        //Check if there is one next question
        if (IsLastQuestion()) {
            EndQuestionPart();
        }
        else{
            questionInfo.SetNextQuestion();
            currentQuestion = questionInfo.GetCurrentQuestion();
            StartCoroutine(WaitToStartNextQuestion());
        }
    }
    private void GiveAudioFeedback(bool isRight)
    {
        if(AudioController.instance != null)
        {
            if (isRight){
                AudioController.instance.PlayRightOnce();
            }
            else {
                AudioController.instance.PlayWrongOnce();
            }
        }
    }
    private bool IsLastQuestion()
    {
        if (currentQuestion + 1 == numberOfQuestions){
            return true;
        }
        return false;
    }
    private IEnumerator WaitToStartNextQuestion()
    {
        yield return new WaitForSeconds((timeBetweenQuestions *2)/3);
        hudController.HideHUD();
        yield return new WaitForSeconds(timeBetweenQuestions/3);
        hudController.ResetVisualFeedback();
        hudController.ResetQuestionInfo();
        StartQuestion(currentQuestion);
    }
    private void EndQuestionPart()
    {
        StartCoroutine(WaitToEndQuizPart());
    }
    private IEnumerator WaitToEndQuizPart()
    {
        yield return new WaitForSeconds(timeAfterQuiz);
        hudController.StopTimeCount();
        hudController.ResetVisualFeedback();
        hudController.ResetQuestionInfo();
        hudController.HideHUD();

        //End the quiz part
        quizPart = false;
        quizStarted = false;

        //Start the bonus part
        bonusPart = true;
        bonusStarted = false;

        questionInfo.cancelClock = true;
    }
    #endregion

    #region Bonus
    private void StartBonus()
    {
        gameObject.GetComponent<BonusController>().StartBonus();
        if(AudioController.instance != null)
            AudioController.instance.TransitionBgToBonus();
    }
    #endregion

    //TODO - MELHORAR
    #region End
    private void CalculatePoints()
    {
        //get points
        int p = hudController.GetPoints();

        //Calculate bonus by time if the player answer at least one right
        if (p != 0)
        {
            float timeSinceBegining = hudController.GetTime();
            hudController.AddPoints ((int)timeSinceBegining*196);
        }

        //bonus points
        bonusPoints = gameObject.GetComponent<BonusController>().GetPoints();
        hudController.AddPoints(bonusPoints);
        p = hudController.GetPoints();

    }
    private void ShowResults()
    {
        if(AudioController.instance != null)
            AudioController.instance.PlayRankingOnce();

        endPart = true;
        result.SetActive(true);
        timeResultText.text = hudController.TransformTime(hudController.GetTime());
        pointsResultText.text = hudController.GetPoints().ToString();
        bonusPointsText.text = bonusPoints.ToString();
        rightsResultText.text = rightAnswers.ToString();
        wrongsResultText.text = wrongAnswers.ToString();
        StartCoroutine(EndIE());
    }
    public void EndGame()
    {
        if(AudioController.instance != null)
            AudioController.instance.LowBonusBg(0.1f);
        CalculatePoints();
        ShowResults();
        if(rightAnswers == numberOfQuestions)
        {
            GetComponent<Flashlight>().ComenceFlashLoop();
        }
        //StartCoroutine(SendPoints(PlayerPrefs.GetString("id"), hudController.GetPoints().ToString(), PlayerPrefs.GetString("server_adr")));
    }
    private IEnumerator SendPoints(string id, string points, string serverURL)
    {
        yield return new WaitForSeconds(1.0f);

        WWW request = new WWW(serverURL + "?id=" + id + "&pontos=" + points);
        yield return request;
    }
    private IEnumerator EndIE()
    {
        yield return new WaitForSeconds(5.0f);
        sun.SetActive(false);
        SceneController.instance.FadeIn();
        endImage.gameObject.SetActive(true);
        yield return new WaitForSeconds(10.0f);
        endImage.gameObject.SetActive(false);
        
        if(AudioController.instance != null)
            AudioController.instance.LowBonusBg(0.02f);
    }
    #endregion

    #region LOADING SCENE
    private void Update()
    {
        //If is in the first scene
		if (currentScene == sceneName [0])
		{
            //Show the logo and goes to the next scene after a few seconds
			if (!goForLoading)
           	{
				//Start to load next scene
				SceneController.instance.StartToLoadScene(sceneName[2]);

                goForLoading = true;
				SceneController.instance.GoToScene(sceneName[2]);
            }
        }

		//If is in the loading scene
		if (currentScene == sceneName[2])
		{
			//Show the logo and goes to the next scene after a few seconds
			if (!GoForMainScreen)
			{
				//Start to load next scene
				SceneController.instance.StartToLoadScene(sceneName[1]);

                GoForMainScreen = true;
				StartCoroutine (WaitToGoNextScene(sceneName[1]));
			}
		}
    }
	private IEnumerator WaitToGoNextScene(string scene){
		yield return new WaitForSeconds(timeBeforeGo);
		SceneController.instance.GoToScene(scene);
	}
    #endregion
}