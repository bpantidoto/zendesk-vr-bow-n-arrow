﻿using System.IO;
using UnityEngine;

public class TxtFileLoader : MonoBehaviour
{
    //"/storage/emulated/0/Movies"
    public static void AddTextOnFile(string filePath, string value)
    {
#if UNITY_ANDROID && !UNITY_EDITOR
        if(filePath.Contains(Application.persistentDataPath))
        {
            filePath = "/storage/emulated/0/Movies"+ filePath.Replace(Application.persistentDataPath,"");
            //filePath = Application.dataPath + filePath.Replace(Application.persistentDataPath, "");
            Debug.Log("FilePath = " + filePath);
        }
#else
        if(filePath.Contains(Application.persistentDataPath))
        {
            filePath = Application.dataPath+ filePath.Replace(Application.persistentDataPath,"");
            //filePath = Application.dataPath + filePath.Replace(Application.persistentDataPath, "");
            Debug.Log("FilePath = " + filePath);
        }
#endif


        try
        {
            Create(filePath, Load(filePath) + "\n" + value);
        }
        catch
        {
            Create(filePath, value);
        }
    }

    public static string Create(string filePath,string value)
    {
        File.WriteAllText(filePath, value);
        return File.ReadAllText(filePath);
    }

    public static string Load(string filePath)
    {
        return File.ReadAllText(filePath);
    }
}
