﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class InactiveRestart : MonoBehaviour {

	public GameObject objectOne;
	public GameObject objectTwo;

	//Reset when inactive
	private void OnApplicationPause(bool paused){
		if(paused){
			//Find the "DontDestroyOnLoad" objects
			objectOne = GameObject.Find("AudioController");
			objectTwo = GameObject.Find("QuestionController");

			//Destroy the "DontDestroyOnLoad" objects
			DestroyObject(objectOne);
		    DestroyObject(objectTwo);

			//Load First Scene
			SceneManager.LoadScene(0);
		}
	}
}