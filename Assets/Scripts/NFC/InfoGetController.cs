﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;
using System.Collections;
using UnityEngine.SceneManagement;

public class InfoGetController : MonoBehaviour {

	public Text output;
    AndroidJavaClass playerClass;
    Dictionary<string, string> userDictionary;
    public float timeBeforeGo;

    void Start()
    {
        output.text = "Aguardando jogador...";

        playerClass = new AndroidJavaClass("com.plugins.NFCUnityActivity"); //Declare o player alterado       
        playerClass.SetStatic<string>("gameObjectName", "mCam"); //o game object onde ficara o callback da leitura do NFC
        playerClass.SetStatic<string>("callbackName", "NFCRecebido"); // o nome do callback
    }

    void NFCRecebido(string msg)
    {
        userDictionary = TratarNfcResult(msg);
        if (!IsSameId())
        {
            StartCoroutine(StartGame());
        }
        else
        {
            output.text = "Aguardando jogador....";
        }
    }

    private bool IsSameId()
    {
        if (userDictionary["id"] == PlayerPrefs.GetString("id"))
        {
            return true;
        }
        return false;
    }

    private IEnumerator StartGame()
    {
        //Save user info.
        PlayerPrefs.SetString("id", userDictionary["id"]);
        PlayerPrefs.SetString("name", userDictionary["name"]);
        PlayerPrefs.SetString("server_adr", userDictionary["server_adr"]);

        output.text = "Bem vindo, " + userDictionary["name"];

        yield return new WaitForSeconds(timeBeforeGo);
        SceneManager.LoadScene(1);
    }


    //---Feito pelo Bruno---
    //Para tratar a leitura. 
    //A leitura da mensagem de NFC chega com o seguinte padrão: id;nome;endereço_do_server_script
    static Dictionary<string, string> TratarNfcResult(string leitura)
    {
        Dictionary<string, string> dic = new Dictionary<string, string>();
        string[] crop = leitura.Split(';');

        if (crop.Length == 3)
        {
            dic.Add("id", crop[0]);
            dic.Add("name", crop[1]);
            dic.Add("server_adr", crop[2]);
            return dic;
        }
        else
        {
            return null;
        }
    }
}