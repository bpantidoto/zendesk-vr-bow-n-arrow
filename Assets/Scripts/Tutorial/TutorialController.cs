﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class TutorialController : MonoBehaviour {
   
    [Header("--Steps--")]
    public int currentTutorialStep;
    public int totalTutorialSteps;
   
    [Header("--Text--")]
    public Text stepText;
    public string[] textOfStep;
    public string finalText;

    [Header("--Targets--")]
    public GameObject[] targetArray;
    public int[] indexTargetSteps;
    public float[] timeBeforeShowTargets;

    #region Step
    public int GetStep()
    {
        return currentTutorialStep;
    }
    public void NextStep()
    {
        currentTutorialStep++;
    }
    public bool IsLastStep()
    {
        bool x = ((totalTutorialSteps - 1) <= currentTutorialStep);
        return x;
    }
    #endregion
    
    #region Time
    public float GetTimeBeforeShowTarget()
    {
        return timeBeforeShowTargets[currentTutorialStep];
    }
    #endregion

    #region Text
    public void SetText(string text)
    {
        stepText.text = text.ToString();
    }
    public void ShowText()
    {
        stepText.gameObject.SetActive(true);
    }
    public void HideText()
    {
        stepText.gameObject.SetActive(false);
    }
    public string GetText(int step)
    {
        return textOfStep[step];
    }
    #endregion

    #region Targets
    public void ShowTarget(int step)
    {
        StartAnimationToShowTarget(targetArray[indexTargetSteps[step]]);
    }
    private void StartAnimationToShowTarget(GameObject target)
    {
        Animator animator = target.GetComponent<Animator>();
        StartCoroutine(WaitToAnimateAble(animator));
    }
    public void HideAllOnTargets()
    {
        for (int i = 0; i < targetArray.Length; i++){
            if(targetArray[i].activeSelf){
                StartCoroutine(WaitToAnimateUnable(targetArray[i].GetComponent<Animator>()));
            }
        }
    }
    #endregion

    #region Collision
    public void TargetCollision(GameObject target)
    {
        //When the target is hit, take it off
        StartCoroutine(WaitToAnimateUnable(target.GetComponent<Animator>()));
        //End the step
        gameObject.GetComponent<GameController>().EndStep();
    }
    public bool IsTargetClearExcluding(GameObject target)
    {
        for (int i = 0; i < targetArray.Length; i++){
            if(targetArray[i].activeSelf)
            {
                if(targetArray[i] == target)
                {
                    return true;
                }
                return false;
            }
        }
        return true;
    }
    #endregion

    private IEnumerator WaitToAnimateAble(Animator animator)
    {
        animator.gameObject.SetActive(true);
        animator.SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("able", false);
    }
    private IEnumerator WaitToAnimateUnable(Animator animator)
    {
        animator.SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        animator.gameObject.SetActive(false);
    }
}