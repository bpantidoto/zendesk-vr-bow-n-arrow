﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Flashlight : MonoBehaviour 
{
	AndroidJavaObject camera=null;
	AndroidJavaObject cameraParameters=null;

	int _numLoops = 7;
	int _overallLoops = 2;

	public void ComenceFlashLoop()
	{
		StartCoroutine(FlashLoop());
	}

	private IEnumerator FlashLoop()
	{
		yield return new WaitForSeconds(2f);
		for(int j = 0; j < _overallLoops; j++)
		{
			for(int i = 0; i< _numLoops; i++)
			{
				ToggleAndroidFlashlight();
				yield return new WaitForSeconds(0.2f);
			}

			yield return new WaitForSeconds(0.5f);
			for(int i = 0; i< _numLoops; i++)
			{
				ToggleAndroidFlashlight();
				yield return new WaitForSeconds(0.2f);
			}

			yield return new WaitForSeconds(0.5f);
			for(int i = 0; i< _numLoops; i++)
			{
				ToggleAndroidFlashlight();
				yield return new WaitForSeconds(0.2f);
			}

			ToggleAndroidFlashlight();
			yield return new WaitForSeconds(1f);
		}
	}
	
	private void ToggleAndroidFlashlight()
	{
		if (camera == null)
		{
			AndroidJavaClass cameraClass = new AndroidJavaClass("android.hardware.Camera"); 
			WebCamDevice[] devices = WebCamTexture.devices;

			camera = cameraClass.CallStatic<AndroidJavaObject>("open", 0); 
			if (camera != null)
			{
				cameraParameters = camera.Call<AndroidJavaObject>("getParameters");
				cameraParameters.Call("setFlashMode","torch"); 
				camera.Call("setParameters",cameraParameters); 
				camera.Call("startPreview");
			}       
		}
		else
		{
			cameraParameters = camera.Call<AndroidJavaObject>("getParameters");
			string flashmode = cameraParameters.Call<string>("getFlashMode");
			if(flashmode!="torch")
				cameraParameters.Call("setFlashMode","torch"); 
			else
				cameraParameters.Call("setFlashMode","off"); 

			camera.Call("setParameters",cameraParameters);
			camera.Call("startPreview"); 
		}
	}

	private void ReleaseAndroidJavaObjects()
	{
		if (camera != null)
		{
			camera.Call("release");
			camera = null;
		}
	}

	private void OnDestroy() 
	{
		ReleaseAndroidJavaObjects();	
	}
}
