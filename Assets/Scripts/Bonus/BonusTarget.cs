﻿using UnityEngine;
using System.Collections;

public class BonusTarget : MonoBehaviour
{
    public BonusController bonusController;
    public int index;

    //If the arrow collides with the target
    public void OnCollisionEnter(Collision collision)
    {
        //Check if it is the arrow
        if (collision.gameObject.tag == "Arrow")
        {
            //Audio
            AudioController.instance.PlayRightBonusOnce();

            //Send a message to the TutorialController
            bonusController.TargetCollision(gameObject);
        }
    }
}