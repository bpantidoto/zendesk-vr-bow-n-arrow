﻿using UnityEngine;
using System.Collections;

public class AnswerController : MonoBehaviour
{
    public GameObject gameController;
    public bool choosen;
    public int alternative; //0 = A, 1 = B, 2 = C or 3 = D
    public GameObject[] alternativeObject;
    public GameObject effect;
    public GameObject effectOrigin;

    private void Start()
    {
        //Finds the gameController
        gameController = GameObject.FindGameObjectWithTag("GameController");
    }

    //Set this object and the alternative that it represents ACTIVE
    public void SetChoosen(int letter)
    {
        choosen = true;
		alternative = letter;
        for (int i = 0; i < alternativeObject.Length; i++)
        {
            alternativeObject[i].SetActive(false);
        }
		alternativeObject[letter].SetActive(true);
    }

    //If the arrow collides with the answer
    public void OnCollisionEnter(Collision collision)
    {
        //Check if it is the arrow
        if (collision.gameObject.tag == "Arrow") {

            //Send a message to the gameController
            gameController.GetComponent<GameController>().AnswerChoosed(alternative);
        }
    }
}
