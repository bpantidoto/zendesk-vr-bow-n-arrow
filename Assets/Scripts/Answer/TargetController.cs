﻿using UnityEngine;
using System.Collections;

public class TargetController : MonoBehaviour {

    [Header("--Target Objects--")]
    public GameObject[] targetArray;
    private GameObject[] randomTargetsArray;

    [Header("--Animators--")]
    public Animator[] targetAnimatorArray;
    private Animator[] activeTargetsAnimatorArray;

    [Header("--Info.--")]
    private bool finishShowing;
    private bool finishHiding;


    public void GenerateRandomTargetArray(int quantity)
    {
        //Create a new array with the given number of questions 
        randomTargetsArray = new GameObject[quantity];

        activeTargetsAnimatorArray = new Animator[quantity];

        int[] indexesOfNewArray = CreateRandomArrayOfInt(quantity, targetArray.Length);
        for (int i = 0; i < indexesOfNewArray.Length; i++)
        {
            randomTargetsArray[i] = targetArray[indexesOfNewArray[i]];
            activeTargetsAnimatorArray[i] = targetAnimatorArray[indexesOfNewArray[i]];
        }
    }

    public void ShowRandomTargets()
    {
        //For each of them
        for (int i = 0; i < randomTargetsArray.Length; i++)
        {
            //--Set them
            randomTargetsArray[i].GetComponent<AnswerController>().SetChoosen(i);

            //--Enable them
            randomTargetsArray[i].SetActive(true);

            //--Show them
            activeTargetsAnimatorArray[i].SetBool("able", true);
            StartCoroutine(WaitToAnimateAble(activeTargetsAnimatorArray[i]));
        }
    }
    private IEnumerator WaitToAnimateAble(Animator animator){
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("able", false);
    }

    public void HideRandomTargets()
    {
        //For each of them
        for (int i = 0; i < randomTargetsArray.Length; i++)
        {
            //--Hide them
            activeTargetsAnimatorArray[i].SetBool("unable", true);
            StartCoroutine(WaitToAnimateUnable(activeTargetsAnimatorArray[i]));
        }

    }
    private IEnumerator WaitToAnimateUnable(Animator animator){
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        animator.gameObject.SetActive(false);
    }
    
    #region RandomArrayGenerator
    //Create an array from other, randomizing what indexes will go to the new one
    private int[] CreateRandomArrayOfInt(int newArrayLenght, int originalArrayLenght)
    {
        int[] newArray = new int[newArrayLenght];

        //Sort the numbers
        for (int i = 0; i < newArray.Length; i++)
        {
            newArray[i] = Random.Range(0, originalArrayLenght - 1);
        }

        //While the array is not truly random
        while (!IsArrayRandom(newArray))
        {
            //Change the numbers of the repeated ones
            for (int i = 0; i < newArray.Length; i++)
            {
                if (IsNumberDuplicated(newArray[i], i, newArray))
                {
                    if (newArray[i] >= (originalArrayLenght - 1)){
                        newArray[i] = 0;
                    }
                    else{
                        newArray[i] += 1;
                    }
                }
            }
        }
        return newArray;
    }
    private bool IsArrayRandom(int[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (IsNumberDuplicated(array[i], i, array))
            {
                return false;
            }
        }
        return true;
    }
    private bool IsNumberDuplicated(int currentNumber, int currentIndex, int[] arrayOfNumbers)
    {
        //Go index by index seeing if there is any number repeated
        for (int j = 0; j < arrayOfNumbers.Length; j++)
        {
            if (currentNumber == arrayOfNumbers[j] && currentIndex != j)
            {
                return true;
            }
        }
        return false;
    }
    #endregion
}
