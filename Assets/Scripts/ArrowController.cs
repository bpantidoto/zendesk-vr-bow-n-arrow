﻿using UnityEngine;
using System.Collections;

public class ArrowController : MonoBehaviour {

    [Header("--GameObject Info.--")]
    private Rigidbody rb;
    private Transform tr;
    public GameObject effect;

    [Header("--Arrow Info.--")]
    public float timeBeforeDestroy;
	public float multiplier;

	private void Start()
	{
		//Set the gameObject Configurations
		rb = gameObject.GetComponent<Rigidbody>();
		tr = gameObject.GetComponent<Transform>();

		//Starts with the acceleration defined by the player power
		rb.velocity = transform.TransformDirection(Vector3.forward * multiplier);
	}

	private void OnCollisionEnter(Collision collision)
    {
		//Ignore other arrows collider

        if (collision.gameObject.name == "Limit")
        {
            //Make a sound
            if(AudioController.instance != null)
            {
                AudioController.instance.PlayWrongOnce();
            }
            

            //Destroy after <timeBeforeDestroy>
            rb.isKinematic = true;
            DestroyObject(gameObject, timeBeforeDestroy);
        }
        else if (collision.gameObject.tag != "Arrow")
		{
			//Make a sound
            if(AudioController.instance != null)
            {
                AudioController.instance.PlayArrowImpactOnce();
            }

            //Destroy after <timeBeforeDestroy>
            rb.isKinematic = true;
			DestroyObject(gameObject, timeBeforeDestroy);

            //Effect
            effect.SetActive(true);
        }
		else
		{
            Physics.IgnoreCollision(gameObject.GetComponent<Collider>(), collision.gameObject.GetComponent<Collider>());
		}
	}
}