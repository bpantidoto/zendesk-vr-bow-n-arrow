﻿using UnityEngine;
using System.Collections;

public class Bow : MonoBehaviour {

	public void PlayPull()
    {
        if(AudioController.instance != null)
        {
            AudioController.instance.PlayArrowPullOnce();
        }
    }

    public void DestroyThis()
    {
        Destroy(gameObject);
    }
}
