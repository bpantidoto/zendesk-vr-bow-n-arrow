﻿using UnityEngine;
using System.Collections;

public class QuestionModel : MonoBehaviour {

    [Header("--Questions--")]
    public Question[] originalQuestionArray;
    public Question[] randomQuestionArray;
    private int numberOfQuestions;
    private int numberCurrentQuestion;

    [Header("--Time--")]
    public float timeSinceStart;
    public float limitTimeCurrent;
    public float timeStartClock;
    public bool countingTime;
    public bool clockTik;
    public bool cancelClock;


    public void GenerateRandomQuestionArray(int numQuestions)
    {
        if(QuestionController.instance != null)
        {
            //Save the number of questions
            numberOfQuestions = numQuestions;

            //Save the original question array
            originalQuestionArray = QuestionController.instance.questionArray;

            //Create a new array with the given number of questions 
            randomQuestionArray = new Question[numberOfQuestions];
            int[] indexesOfNewArray = CreateRandomArrayOfInt(numberOfQuestions, originalQuestionArray.Length);
            for (int i = 0; i < numberOfQuestions; i++){
                randomQuestionArray[i] = originalQuestionArray[indexesOfNewArray[i]];
            }    
        }
    }

    public void DontRandomize(int numQuestions)
    {
        if(QuestionController.instance != null)
        {
            randomQuestionArray = QuestionController.instance.questionArray;
        }
    }

    public string GetQuestionTitle(int currentQuestion){
        return randomQuestionArray[currentQuestion].title;
    }
    public string GetQuestionAnswer(int currentQuestion, int currentAnswer){
        return randomQuestionArray[currentQuestion].answerArray[currentAnswer];
    }
    public string[] GetAllAnswers(int currentQuestion)
    {
        string[] allAnswers = new string[randomQuestionArray[currentQuestion].answerArray.Length];
        for (int i = 0; i < allAnswers.Length; i++){
            allAnswers[i] = GetQuestionAnswer(currentQuestion, i);
        }
        return allAnswers;
    }
    public int GetQuestionRightIndex(int currentQuestion){
        return randomQuestionArray[currentQuestion].rightAnswer;
    }
    public string GetQuestionRightText(int currentQuestion)
    {
        return randomQuestionArray[currentQuestion].answerArray[randomQuestionArray[currentQuestion].rightAnswer];
    }
    public bool IsAnswerRight(int currentQuestion, string choosenAnswer)
    {
        string rightAnswer = randomQuestionArray[currentQuestion].answerArray[randomQuestionArray[currentQuestion].rightAnswer];
        if (rightAnswer == choosenAnswer){
            return true;
        }
        return false;
    }
    public bool IsAnswerRight(int currentQuestion, int choosenAnswer)
    {
        int rightAnswer = randomQuestionArray[currentQuestion].rightAnswer;
        if (rightAnswer == choosenAnswer)
        {
            return true;
        }
        return false;
    }

    public int GetCurrentQuestion()
    {
        return numberCurrentQuestion;
    }
    public void SetCurrentQuestion(int x)
    {
        numberCurrentQuestion = x;
    }
    public void SetNextQuestion()
    {
        numberCurrentQuestion++;
    }
    public int GetNumberOfQuestions()
    {
        return numberOfQuestions;
    }

    public void StartCountingQuestionTime(float currentTime, float limitTime, float timeStartClock)
    {
        this.timeStartClock = timeStartClock;
        timeSinceStart = currentTime;
        limitTimeCurrent = limitTime;
        cancelClock = false;
        countingTime = true;
        clockTik = false;
    }
    public void Update()
    {
        //If the time is counting
        if (countingTime && !cancelClock)
        {
            //Time to stat clock sound
            if (timeSinceStart + timeStartClock <= Time.time && !clockTik)
            {
                clockTik = true;
                if(AudioController.instance != null)
                    AudioController.instance.PlayClockOnce(10);
            }

            //Time to end current question
            if (timeSinceStart + limitTimeCurrent <= Time.time)
            {
                countingTime = false;
                //gameObject.GetComponent<GameController>().AnswerChoosed(4);
                gameObject.SendMessage("AnswerChoosed",4,SendMessageOptions.DontRequireReceiver);
            }
        }
    }


    #region PRINT - for test only
    public void PrintRandomQuestionArray()
    {
        for (int i = 0; i < numberOfQuestions; i++)
        {
            print("question [" + i + "] --> " + randomQuestionArray[i].title);
            for (int j = 0; j < randomQuestionArray[i].answerArray.Length; j++)
            {
                print("question [" + i + "][" + j + "]: " + randomQuestionArray[i].answerArray[j]);
            }
            print("RightAnswer [" + i + "]: (" + randomQuestionArray[i].rightAnswer + ") --> " + randomQuestionArray[i].answerArray[randomQuestionArray[i].rightAnswer]);
        }
    }
    public void PrintRandomQuestionArrayFromMethods()
    {
        for (int i = 0; i < numberOfQuestions; i++)
        {
            print("question [" + i + "] --> " + GetQuestionTitle(i));
            for (int j = 0; j < randomQuestionArray[i].answerArray.Length; j++)
            {
                print("question [" + i + "][" + j + "]: " + GetQuestionAnswer(i, j));
            }
            print("RightAnswer [" + i + "]: (" + GetQuestionRightIndex(i) + ") --> " + GetQuestionRightText(i));
        }
    }
    #endregion

    #region RandomArrayGenerator
    //Create an array from other, randomizing what indexes will go to the new one
    private int[] CreateRandomArrayOfInt(int newArrayLenght, int originalArrayLenght)
    {
        int[] newArray = new int[newArrayLenght];
        //Sort the numbers
        for (int i = 0; i < newArray.Length; i++)
        {
            newArray[i] = Random.Range(0, originalArrayLenght - 1);
        }

        //While the array is not truly random
        while (!IsArrayRandom(newArray))
        {
            //Change the numbers of the repeated ones
            for (int i = 0; i < newArray.Length; i++)
            {
                if (IsNumberDuplicated(newArray[i], i, newArray))
                {
                    if (newArray[i] >= (originalArrayLenght - 1)) { newArray[i] = 0; }
                    else { newArray[i] += 1; }
                }
            }
        }
        return newArray;
    }
    private bool IsArrayRandom(int[] array)
    {
        for (int i = 0; i < array.Length; i++)
        {
            if (IsNumberDuplicated(array[i], i, array))
            {
                return false;
            }
        }
        return true;
    }
    private bool IsNumberDuplicated(int currentNumber, int currentIndex, int[] arrayOfNumbers)
    {
        //Go index by index seeing if there is any number repeated
        for (int j = 0; j < arrayOfNumbers.Length; j++)
        {
            if (currentNumber == arrayOfNumbers[j] && currentIndex != j)
            {
                return true;
            }
        }
        return false;
    }
    #endregion
}