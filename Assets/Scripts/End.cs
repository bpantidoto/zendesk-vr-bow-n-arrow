﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class End : MonoBehaviour 
{
    private GameObject objectOne;
    private GameObject objectTwo;

    private void Update () 
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            //Find the "DontDestroyOnLoad" objects
            objectOne = GameObject.Find("AudioController");
            objectTwo = GameObject.Find("QuestionController");

            //Destroy the "DontDestroyOnLoad" objects
            DestroyObject(objectOne);
            DestroyObject(objectTwo);

            //Load First Scene
            SceneManager.LoadScene(0);
        }
    }
}