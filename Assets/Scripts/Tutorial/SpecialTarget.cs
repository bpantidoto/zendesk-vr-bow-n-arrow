﻿using UnityEngine;
using System.Collections;

public class SpecialTarget : MonoBehaviour
{
    public int alternative; //0 = A, 1 = B, 2 = C or 3 = D
    public GameObject[] alternativeObject;
    public TutorialController tutorialController;

    //Show with alternative letter
    public void SetTargetAlternative(int _alternative)
    {
        this.alternative = _alternative;
        for (int i = 0; i < alternativeObject.Length; i++)
        {
            alternativeObject[i].SetActive(false);
        }
        alternativeObject[_alternative].SetActive(true);
    }

    //If the arrow collides with the answer
    public void OnCollisionEnter(Collision collision)
    {
        //Check if it is the arrow
        if (collision.gameObject.tag == "Arrow")
        {
            //Audio
            if(AudioController.instance != null)
            {
                AudioController.instance.PlayRightBonusOnce();
            }
            
            //Send a message to the TutorialController
            tutorialController.TargetCollision(gameObject);
        }
    }
}