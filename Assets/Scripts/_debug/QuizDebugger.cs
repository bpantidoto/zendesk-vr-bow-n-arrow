﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.SceneManagement;

public class QuizDebugger : MonoBehaviour
{
    [Header("--Result info--")]
    public GameObject result;
    public Text timeResultText;
    public Text rightsResultText;
    public Text wrongsResultText;
    public Text pointsResultText;
    public Text bonusPointsText;
    public int bonusPoints;
    public int rightAnswers;
    public int wrongAnswers;
    
    [Header("--Quiz Info--")]
    public int numberOfQuestions;
    public int numberOfAnswers;
    private QuestionModel questionInfo;
    private HUDController hudController;
    private TargetController targetController;
    private int currentQuestion;
    public float timeAfterQuiz;
    public float limitTimeQuizQuestion;
    public float timeStartClock;

    [Header("--QA Controller--")]
    public bool canShoot; //TRUE when the player can shoot (NEEDS TO BE PUBLIC)     
    public int pointsByAnswer;
    public float timeBetweenQuestions;
    public int maxTimeInMilliseconds;
    public int timeMultiplier;

    [Header("--Flow Controller--")]
    public bool introPart;
    public bool introStarted;
    public bool lastIntroPart;
    public bool tutorialPart;
    public bool tutorialStarted;
    public bool quizPart;
    public bool quizStarted;
    public bool bonusPart;
    public bool bonusStarted;
    public bool endPart;
    public float timeBeforeIntro;


    private void Start()
    {
       SetupQuizScene();
	   StartQuiz();
    }

	private void Update() 
	{
		if(Input.GetKeyUp(KeyCode.N))
		{
			NextQuestion();
		}
	}

    private void SetupQuizScene()
    {
        //Init the QuestionView (To get information about the questions)
        questionInfo = gameObject.GetComponent<QuestionModel>();

        //Generate the Questions that will be used in this game
        questionInfo.DontRandomize(numberOfQuestions);

        //Init the HUDController 
        hudController = gameObject.GetComponent<HUDController>();

        //zero points
        hudController.SetPoints(0);

        //reset time
        hudController.ResetTime();
    }

    private void StartQuiz()
    {
        lastIntroPart = false;

        //Start in the first question
        currentQuestion = questionInfo.GetCurrentQuestion();
        //Start time count
        //hudController.StartTimeCount();

        //Start the first question
        StartQuestion(currentQuestion);
    }

    private void StartQuestion(int question)
    {
        //--HUD--
        //Show the HUD
        hudController.ShowHUD();
        //Show question
        hudController.SetQuestionTitle(questionInfo.GetQuestionTitle(currentQuestion));
        //Set Answers
        hudController.SetAnswers(questionInfo.GetAllAnswers(currentQuestion));
        //Show the current question feedback
        hudController.SetNumQuestionFeedBack(currentQuestion + 1, numberOfQuestions);

        //questionInfo.StartCountingQuestionTime(Time.time, limitTimeQuizQuestion, timeStartClock);
        //hudController.StartTimeCountQuestion();
    } 

    public void AnswerChoosed(int alternative)
    {
        questionInfo.cancelClock = true;

        //check if it was the right answer
        bool isRight = questionInfo.IsAnswerRight(currentQuestion, alternative);

        //--FEEDBACK--
        hudController.SetVisualFeedBack(questionInfo.GetQuestionRightIndex(currentQuestion), alternative);

        //--POINTS--
        float questionTime = hudController.EndTimeCountQuestion();
        float bonusPoints = (28.0f - questionTime) * 296;

        if (isRight){
            hudController.AddPoints(pointsByAnswer);
            hudController.AddPoints((int)bonusPoints);
            rightAnswers++;
            
            TxtFileLoader.AddTextOnFile(Application.persistentDataPath + "/DB_Text.txt", questionInfo.GetQuestionTitle(currentQuestion) +"\n  Correto");
        }
        else{
            wrongAnswers++;
            TxtFileLoader.AddTextOnFile(Application.persistentDataPath + "/DB_Text.txt", questionInfo.GetQuestionTitle(currentQuestion) + "\n  Errado");
        }

		/*
        //Check if there is one next question
        if (IsLastQuestion()) {
            EndQuestionPart();
        }
        else{
            questionInfo.SetNextQuestion();
            currentQuestion = questionInfo.GetCurrentQuestion();
            StartCoroutine(WaitToStartNextQuestion());
        }
		*/
    }

    private bool IsLastQuestion()
    {
        if (currentQuestion + 1 == numberOfQuestions){
            return true;
        }
        return false;
    }
    private IEnumerator WaitToStartNextQuestion()
    {
        yield return new WaitForSeconds((timeBetweenQuestions *2)/3);
        hudController.ResetVisualFeedback();
        hudController.ResetQuestionInfo();
        hudController.HideHUD();
        yield return new WaitForSeconds(timeBetweenQuestions/3);
        StartQuestion(currentQuestion);
    }

	private void NextQuestion()
	{
		questionInfo.SetNextQuestion();
        currentQuestion = questionInfo.GetCurrentQuestion();
		hudController.ResetVisualFeedback();
        hudController.ResetQuestionInfo();
        hudController.HideHUD();
		StartQuestion(currentQuestion);
	}

    private void EndQuestionPart()
    {
        StartCoroutine(WaitToEndQuizPart());
    }

    private IEnumerator WaitToEndQuizPart()
    {
        yield return new WaitForSeconds(timeAfterQuiz);
        hudController.StopTimeCount();
        hudController.ResetVisualFeedback();
        hudController.ResetQuestionInfo();
        hudController.HideHUD();

        //End the quiz part
        quizPart = false;
        quizStarted = false;

        //Start the bonus part
        bonusPart = true;
        bonusStarted = false;

        questionInfo.cancelClock = true;
    }
}