﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class HUDController : MonoBehaviour {

    public GameObject hud;

    [Header("--Question UI--")]
    public Text questionText;
    public Text[] answerText;

    [Header("--Feedback TEXT--")]
    public Text numQuestionsText; //current/total
    public Text numPointsText;
    public Text numTimeText;
    public Text playerNameText;

    [Header("--Feedback IMAGES--")]
    public Image[] choosedFeedbackImage;
    public Image[] rightFeedbackImage;
    public Image[] wrongFeedbackImage;

    [Header("--Information--")]
    private int playerPoints;

    [Header("--Time--")]
    private bool countTime;
    private float initialTime;
    private float currentTime;
    private float timeSinceStart;

    [Header("--Time Question--")]
    private float initialTimeQuestion;
    private float totalTimeQuestion;

    private QuestionCanvasAnimation _canvasAnimator;

    private void Start()
    {
        if(hud != null)
        {
            _canvasAnimator = hud.GetComponent<QuestionCanvasAnimation>();
        }
    }

    private void Update()
    {
        //Updates the time - if it already started
        if (countTime)
        {
            currentTime = Time.time;
            timeSinceStart = currentTime - initialTime;
            ShowTime();
        }
    }

    #region Questions and Answers 
    public void SetQuestionTitle(string questionTitle)
    {
        questionText.text = questionTitle.ToString();
    }
    public void SetAnswers(string[] answerArray)
    {
        for(int i = 0; i < answerText.Length; i++){
            answerText[i].text = answerArray[i].ToString();
        }
    }
    public void ResetQuestionInfo()
    {
        questionText.text = "";
        for (int i = 0; i < answerText.Length; i++){
            answerText[i].text = "";
        }
    }
    public void SetNumQuestionFeedBack(int currentNumber, int totalNumber)
    {
        numQuestionsText.text = currentNumber + "/" + totalNumber;
    }
    #endregion

    #region Feedback
    public void SetVisualFeedBack(int rightAnswer, int userAnswer)
    {
        if (userAnswer < 4)
        {
            choosedFeedbackImage[userAnswer].gameObject.SetActive(true);
            rightFeedbackImage[rightAnswer].gameObject.SetActive(true);
            if (rightAnswer != userAnswer)
            {
                wrongFeedbackImage[userAnswer].gameObject.SetActive(true);
            }
        }
    }
    public void ResetVisualFeedback()
    {
        for (int i = 0; i < rightFeedbackImage.Length; i++){
            rightFeedbackImage[i].gameObject.SetActive(false);
            wrongFeedbackImage[i].gameObject.SetActive(false);
            choosedFeedbackImage[i].gameObject.SetActive(false);
        }
    }
    #endregion

    #region Username
    public void SetPlayerName(string name)
    {
        //NFC
    }
    #endregion

    #region Points
    public void AddPoints(int points)
    {
        playerPoints += points;
        numPointsText.text = playerPoints.ToString();
    }
    public void SetPoints(int points)
    {
        playerPoints = points; 
        numPointsText.text = playerPoints.ToString();
    }
    public int GetPoints()
    {
        return playerPoints;
    }
    #endregion

    #region Time
    public void StartTimeCount()
    {
        initialTime = Time.time;
        countTime = true;
    }
    private void ShowTime()
    {
        numTimeText.text = TransformTime(timeSinceStart);
    }
    public string TransformTime(float totalSeconds)
    {
        string formatedString;
        int seconds = (int)(totalSeconds % 60);
        int minutes = (int)(totalSeconds / 60);
        string secondsString = seconds.ToString();
        string minutesString = minutes.ToString();

        if (seconds < 10){ secondsString = "0" + seconds.ToString(); }
        if (minutes < 10){ minutesString = "0" + minutes.ToString(); }

        formatedString = minutesString + ":" + secondsString;
        return formatedString;
    }
    public void StopTimeCount()
    {
        countTime = false;
    }
    public void ResetTime()
    {
        countTime = false;
        initialTime = 0;
        currentTime = 0;
        timeSinceStart = 0;
        ShowTime();
    }
    public float GetTime()
    {
        return timeSinceStart;
    }
    #endregion

    #region Time Slice
    public void StartTimeCountQuestion()
    {
        initialTimeQuestion = Time.time;
    }
    public float EndTimeCountQuestion()
    {
        totalTimeQuestion = Time.time - initialTimeQuestion;
        return totalTimeQuestion;
    }
    #endregion

    

    #region HUD
    public void HideHUD()
    {
        //StartCoroutine(WaitToAnimateUnable());
        _canvasAnimator.AnimateOut();
    }
    public void ShowHUD()
    {
        //StartCoroutine(WaitToAnimateAble());
        _canvasAnimator.AnimateIn();
    }
    private IEnumerator WaitToAnimateAble()
    {
        hud.SetActive(true);
        hud.GetComponent<Animator>().SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        hud.GetComponent<Animator>().SetBool("able", false);
    }
    private IEnumerator WaitToAnimateUnable()
    {
        hud.GetComponent<Animator>().SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        hud.GetComponent<Animator>().SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        hud.SetActive(false);
    }
    #endregion
}