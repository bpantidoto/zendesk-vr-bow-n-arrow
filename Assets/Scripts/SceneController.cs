using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class SceneController : MonoBehaviour {

    [Header("--Fade--")]
    public Image fadeImage;
    public float timeToFade;
    public bool fading;
	public Canvas loading;
	private AsyncOperation ao;

    //static instance - Is not destroyed when a scene changes
    public static SceneController instance;
    private void Awake()
    {
        instance = this;        
    }
    private void Start()
    {
        fadeImage.gameObject.SetActive(true);
    }

    //All the fade images fadein
    public void FadeIn()
    {
        fading = true;
        fadeImage.canvasRenderer.SetAlpha(0.0f);
        fadeImage.CrossFadeAlpha(1.0f, timeToFade, false);
        StartCoroutine(WaitToEndFade());
    }

    //All the fade images fadeout
    public void FadeOut()
    {
        fading = true;
        fadeImage.canvasRenderer.SetAlpha(1.0f);
        fadeImage.CrossFadeAlpha(0.0f, timeToFade, false);
        StartCoroutine(WaitToEndFade());
    }

    //Change the fading boolean when the fade is over
    private IEnumerator WaitToEndFade()
    {
        yield return new WaitForSeconds(timeToFade);
        fading = false;
    }

    //Called when a scene is loaded to FadeOut
    public void OpenScene()
    {
        if (!fading) { FadeOut(); }
    }

    //Go to the scene named after the fade
    public void GoToScene(string scene)
    {
		if (!fading) { FadeIn(); }
        StartCoroutine(WaitToChangeScene(scene));
    }
		
    private IEnumerator WaitToChangeScene(string sceneToGo)
    {
		yield return new WaitForSeconds(timeToFade + (timeToFade / 2));
		ao.allowSceneActivation = true;
    }
		
    //Return the name os the current scene
    public string GetCurrentSceneName()
    {
        return SceneManager.GetActiveScene().name;
    }

	public void StartToLoadScene(string sceneToGo)
	{
		ao = SceneManager.LoadSceneAsync(sceneToGo);
		ao.allowSceneActivation = false;
	}
}