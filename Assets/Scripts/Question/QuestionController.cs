﻿using UnityEngine;
using System.Xml;
using System.Xml.Serialization;
using System.IO;
using System.Collections;

public class Question
{
    public string title;
    public string[] answerArray;
    public int rightAnswer;
}

public class QuestionController : MonoBehaviour {

    [Header("--Text Asset Info.--")]
    public TextAsset textAsset;
    private string stringFromTextAsset;

    [Header("--Questions Info.--")]
    public Question[] questionArray;

    [Header("--XML Paths--")]
    public XmlDocument xmlDoc;
    public string pathToNodeQuestions = "QuestionCollection/Question";
    public string pathToNodeTitle = "Title";
    public string pathToNodeAnswers = "Answer";
    public string pathToNodeRightAnswer = "RightAnswer";
    private string xmlFile;

    private static int playCounter;
    //static instance - Is not destroyed when a scene changes
    public static QuestionController instance;
    private void Awake()
    {
        DontDestroyOnLoad(gameObject);
        instance = this;
    }

    private void Start()
    {
        playCounter = PlayerPrefs.GetInt("playCounter", 0);
        playCounter++;
        PlayerPrefs.SetInt("playCounter", playCounter);
        TxtFileLoader.AddTextOnFile(Application.persistentDataPath + "/DB_Text.txt", "<StartPlay>"+ playCounter);
        //Read and save the XML information
        ReadXmlInfo();
    }

    //Read the XML file ans save the information in the <questionArray>
    private void ReadXmlInfo()
    {
        //read the XML file and save it in a string
        xmlFile = textAsset.text;

        //Init the XMLDocument and load the xmlfile
        xmlDoc = new XmlDocument();
        xmlDoc.LoadXml(xmlFile);

        //START TO READ
        //1 - XmlNodeList -- Questions
        XmlNodeList questionsNodeList = xmlDoc.SelectNodes(pathToNodeQuestions);

        //2 - Init the questionArray with the number of questions in the XML file
        questionArray = new Question[questionsNodeList.Count];

        //3 - For each iten, create: (XmlNode -> Title) | (XMLNodeList -> Answer) | (XmlNode -> RightAnswer)
        for (int i = 0; i < questionsNodeList.Count; i++)
        {
            //3.0 - Init each Question Object
            questionArray[i] = new Question();

            //3.1 - Read the title and Save the title in the array <questionArray>
            XmlNode titleNode = questionsNodeList[i].SelectSingleNode(pathToNodeTitle);
            questionArray[i].title = titleNode.InnerText.ToString();

            //3.3 - Save each answer in the array <answerArray> inside the <questionArray>
            XmlNodeList answerNodeList = questionsNodeList[i].SelectNodes(pathToNodeAnswers);
            questionArray[i].answerArray = new string[answerNodeList.Count];
            for (int j = 0; j < answerNodeList.Count; j++)
            {
                questionArray[i].answerArray[j] = answerNodeList[j].InnerText.ToString(); ;
            }

            //3.2 -  Read the rightAnswer and save the rightAnswer in the array <questionArray>
            XmlNode rightAnswerNode = questionsNodeList[i].SelectSingleNode(pathToNodeRightAnswer);
            questionArray[i].rightAnswer = XmlConvert.ToInt32(rightAnswerNode.InnerText);
        }
    }
}