﻿using UnityEngine;
using System.Collections;

public class InEditorController : MonoBehaviour {

    public float speed;
    public Transform GameCamera;

    private void Update() {
        Vector3 v3 = new Vector3((Input.GetAxis("Vertical") *-1), Input.GetAxis("Horizontal"), 0.0f);
        GameCamera.Rotate(v3 * speed * Time.deltaTime);
    }
}
