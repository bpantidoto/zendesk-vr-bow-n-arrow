﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public class QuestionCanvasAnimation : MonoBehaviour 
{
	public Vector3 visiblePosition;
	public Vector3 invisiblePosition;
	public Ease enteringEase;
	public Ease leavingEase;
	public float animationTime = 1f;


	private void Update() {
		if(Input.GetKeyDown(KeyCode.I))
		{
			AnimateIn();
		}

		if(Input.GetKeyDown(KeyCode.O))
		{
			AnimateOut();
		}
	}

	public void AnimateIn()
	{
		transform.DOLocalMove(visiblePosition, animationTime).SetEase(enteringEase);
	}

	public void AnimateOut()
	{
		transform.DOLocalMove(invisiblePosition, animationTime).SetEase(leavingEase);
	}
}
