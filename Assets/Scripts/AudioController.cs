﻿using UnityEngine;
using System.Collections;

public class AudioController : MonoBehaviour 
{
	public static AudioController instance;
	public AudioClip arrowShoot;
    public AudioClip arrowPull;
    public AudioClip stringFlick;
    public AudioClip arrowImpact;
	public AudioClip right;
	public AudioClip wrong;
    public AudioClip ranking;
    public AudioClip rightBonus;

    public AudioSource clock;
    public AudioSource adBackgroundIntro;
    public AudioSource adBackgroundQuiz;
    public AudioSource adBackgroundBonus;
    private AudioSource ad;

	private void Awake(){
		instance = this;
		DontDestroyOnLoad (this);
	}
	private void Start(){
		ad = gameObject.GetComponent<AudioSource>();
        adBackgroundIntro.gameObject.SetActive(true);
        adBackgroundQuiz.gameObject.SetActive(false);
        adBackgroundBonus.gameObject.SetActive(false);
    }

    public void TransitionBgToQuiz()
    {
        StartCoroutine(TransitionIEQ());
    } //intro to Quiz
    private IEnumerator TransitionIEQ()
    {
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.4f;
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.3f;
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.2f;
        adBackgroundQuiz.volume = 0.1f;
        adBackgroundQuiz.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.2f;
        adBackgroundIntro.volume = 0.1f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.3f;
        adBackgroundIntro.volume = 0.05f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.4f;
        adBackgroundIntro.volume = 0.0f;
        adBackgroundIntro.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.5f;

        adBackgroundQuiz.loop = false;
    }
    public void TransitionBgToIntro()
    {
        StartCoroutine(TransitionIEI());
    } //Quiz to Intro
    private IEnumerator TransitionIEI()
    {
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.4f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.3f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.2f;
        adBackgroundIntro.volume = 0.1f;
        adBackgroundIntro.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.2f;
        adBackgroundQuiz.volume = 0.1f;
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.3f;
        adBackgroundQuiz.volume = 0.05f;
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.4f;
        adBackgroundQuiz.volume = 0.0f;
        adBackgroundQuiz.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        adBackgroundIntro.volume = 0.5f;

        adBackgroundIntro.loop = false;
    }
    public void TransitionBgToBonus()
    {
        StartCoroutine(TransitionIEB());
    } //Quiz to Bonus
    private IEnumerator TransitionIEB()
    {
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.4f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.3f;
        yield return new WaitForSeconds(1f);
        adBackgroundQuiz.volume = 0.2f;
        adBackgroundBonus.volume = 0.1f;
        adBackgroundBonus.gameObject.SetActive(true);
        yield return new WaitForSeconds(1f);
        adBackgroundBonus.volume = 0.2f;
        adBackgroundQuiz.volume = 0.1f;
        yield return new WaitForSeconds(1f);
        adBackgroundBonus.volume = 0.3f;
        adBackgroundQuiz.volume = 0.05f;
        yield return new WaitForSeconds(1f);
        adBackgroundBonus.volume = 0.4f;
        adBackgroundQuiz.volume = 0.0f;
        adBackgroundQuiz.gameObject.SetActive(false);
        yield return new WaitForSeconds(1f);
        adBackgroundBonus.volume = 0.5f;

        adBackgroundBonus.loop = false;
    }

    public void PlaybackgroundQuiz()
    {
        adBackgroundQuiz.Play();
    }
    public void StopAll()
    {
        adBackgroundIntro.Stop();
        adBackgroundQuiz.Stop();
        adBackgroundBonus.Stop();
        ad.Stop();
        clock.Stop();
    }
    public void PlaybackgroundIntro()
    {
        adBackgroundIntro.gameObject.SetActive(true);
        adBackgroundIntro.volume = 0.5f;
        adBackgroundIntro.Play();
    }

    public void PlayArrowShootOnce(){
		ad.priority = 2;
		ad.PlayOneShot (arrowShoot, 0.5f);
	}
    public void PlayArrowPullOnce()
    {
        ad.priority = 3;
        ad.PlayOneShot(arrowPull, 0.2f);
    }
    public void PlayStringFlickOnce()
    {
        ad.priority = 5;
        ad.PlayOneShot(stringFlick, 0.05f);
    }
    public void PlayArrowImpactOnce(){
		ad.priority = 3;
		ad.PlayOneShot(arrowImpact, 0.3f);
	}
	public void PlayRightOnce(){
		ad.priority = 1;
		ad.PlayOneShot(right, 0.8f);
	}
	public void PlayWrongOnce(){
		ad.priority = 1;
		ad.PlayOneShot(wrong, 0.8f);
	}
    public void PlayRankingOnce()
    {
        ad.priority = 2;
        ad.PlayOneShot(ranking, 0.5f);
    }
    public void PlayRightBonusOnce()
    {
        ad.priority = 5;
        ad.PlayOneShot(rightBonus, 0.8f);
    }

    public void LowBonusBg(float vol)
    {
        adBackgroundBonus.volume = vol;
    }

    public void PlayClockOnce(float time)
    {
        clock.Play();
        clock.volume = 0.6f;
        StartCoroutine(GrowVolume(time));
    }
    public void ClockStop()
    {
        clock.Stop();
        StopCoroutine("GrowVolume");
    }
    public IEnumerator GrowVolume(float time)
    {
        float miniTime = time/3;
        yield return new WaitForSeconds(miniTime);
        clock.volume = 0.7f;
        yield return new WaitForSeconds(miniTime);
        clock.volume = 0.8f;
        yield return new WaitForSeconds(miniTime);
        clock.volume = 0.9f;
    }
}