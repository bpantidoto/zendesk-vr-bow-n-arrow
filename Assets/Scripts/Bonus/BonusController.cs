﻿using UnityEngine;
using System.Collections;

public class BonusController : MonoBehaviour {

    [Header("--Intro Info.--")]
    public GameObject intro;

    [Header("--Time Info.--")]
    public float targetTimeLimit;
    public float introTime;

    [Header("--Target Info.--")]
    public GameObject[] targetArray;
    public bool[] targetShooted;

    [Header("--Points--")]
    public int totalPoints;
    public int pointsPerTarget;
    
    public void StartBonus()
    {
        //SetIndexes
        targetShooted = new bool[targetArray.Length];
        for (int i = 0; i < targetArray.Length; i++)
        {
            targetArray[i].GetComponent<BonusTarget>().index = i;
            targetShooted[i] = false;
        }
        StartCoroutine(BonusCoroutine());
    }


    private IEnumerator WaitToAble()
    {
        intro.SetActive(true);
        intro.GetComponent<Animator>().SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        intro.GetComponent<Animator>().SetBool("able", false);
        yield return new WaitForSeconds(introTime);
        intro.GetComponent<Animator>().SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        intro.GetComponent<Animator>().SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        intro.SetActive(false);
    }

    //TODO - MELHORAR
    private IEnumerator BonusCoroutine()
    {
        //Show intro
        StartCoroutine(WaitToAble());
        yield return new WaitForSeconds(introTime + 1.0f);

        //Show Bonus
        Show(0);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(1);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(2);
        Hide(0);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(3);
        Hide(1);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(4);
        Hide(2);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(5);
        Hide(3);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(6);
        Hide(4);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(7);
        Hide(5);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(8);
        Hide(6);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(9);
        Hide(7);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(10);
        Hide(8);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(11);
        Hide(9);
        yield return new WaitForSeconds(targetTimeLimit);
        Show(12);
        Hide(10);
        yield return new WaitForSeconds(targetTimeLimit);
        Hide(11);
        yield return new WaitForSeconds(targetTimeLimit);
        Hide(12);
        yield return new WaitForSeconds(1);
        SetEndBonus();
        yield return null;
    }

    //TODO - MELHORAR
    private void SetEndBonus()
    {
        gameObject.GetComponent<GameController>().EndGame();
    }

    #region Collision
    public void TargetCollision(GameObject target)
    {
        //When the target is hit, take it off
        StartCoroutine(WaitToAnimateUnable(target.GetComponent<Animator>()));

        //Mark the target as shooted
        targetShooted[target.GetComponent<BonusTarget>().index] = true;

        //Add Points
        AddPoints();
    }
    #endregion

    #region Points
    public void AddPoints()
    {
        totalPoints += pointsPerTarget;
    }
    public int GetPoints()
    {
        return totalPoints;
    }
    #endregion

    #region Show and Hide
    private void Show(int i)
    {
        Animator animator = targetArray[i].GetComponent<Animator>();
        StartCoroutine(WaitToAnimateAble(animator));
    }
    private void Hide(int i)
    {
        if (!GetShoot(i))
        {
            Animator animator = targetArray[i].GetComponent<Animator>();
            StartCoroutine(WaitToAnimateUnable(animator));
        }
    }
    private bool GetShoot(int i)
    {
        return targetShooted[i];
    }
    private IEnumerator WaitToAnimateAble(Animator animator)
    {
        animator.gameObject.SetActive(true);
        animator.SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("able", false);
    }
    private IEnumerator WaitToAnimateUnable(Animator animator)
    {
        animator.SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        animator.SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        animator.gameObject.SetActive(false);
    }
    #endregion
}
