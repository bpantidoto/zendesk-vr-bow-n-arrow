﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class ShootController : MonoBehaviour {

    [Header("--GameObject config.--")]
    public GameObject gameControllerObject;
	public Transform trOrigin;
    public Transform trFake;
    public Camera cameraObject;
    public HUDController hudController;
    public GameObject originalArrow;
    public GameObject fakeArrowAnimation;

    [Header("---Bow Shoot Info.--")]
    public GameObject arrowGameObject;
	public Animator bowAnimator;
	public BowOrientation bowOrientation;
	public bool shoot;
    public bool shouldRest;
    public bool canshoot = false;

    [Header("--Cooldown Info--")]
	public float timeBeforeShootAgain;
	public float timeSinceLastShoot;

	private void Update()
	{
        if (shouldRest)
        {
            canshoot = false;
        }else {
            canshoot = gameControllerObject.GetComponent<GameController>().canShoot;
        }

		//PLAYER SHOOTING
		//0 - Check if the player can shoot
		if (canshoot)
		{
			//1 - Check if not in the shoot "cooldown"
			if(Time.time >= timeBeforeShootAgain + timeSinceLastShoot){
				//2 - When the player presses to shoot, shoots
				if (Input.GetButtonUp("Fire1"))
				{
					//3 - Reset the <timeSinceLastShoot>
					timeSinceLastShoot = Time.time;

					//4 - AUDIO start shoot
                    if(AudioController.instance != null)
                    {
                        AudioController.instance.PlayArrowShootOnce();   
                    }

                    //5 - Shoot
                    originalArrow.GetComponent<MeshRenderer>().enabled = false;
                    
                    bowAnimator.SetBool("shoot", true);
					StartCoroutine(WaitToStopShoot());
					StartCoroutine(WaitToShoot());
				}
			}
		}

        //Verify if the archer should rest
        if (gameControllerObject.GetComponent<GameController>().introPart || gameControllerObject.GetComponent<GameController>().lastIntroPart || !bowOrientation.rightArea || gameControllerObject.GetComponent<GameController>().endPart)
        {
            shouldRest = true;
        }
        else
        {
            shouldRest = false;
        }
	}

	private IEnumerator WaitToShoot(){
        Vector3 fakePosition = new Vector3(trFake.position.x, trFake.position.y, trFake.position.z);
        Quaternion fakeRotation = new Quaternion(trFake.rotation.x, trFake.rotation.y, trFake.rotation.z, trFake.rotation.w);
        Instantiate(fakeArrowAnimation, fakePosition, fakeRotation);
        yield return new WaitForSeconds(0.01f);
		Shoot();
	}

	private IEnumerator WaitToStopShoot(){
        yield return new WaitForSeconds(0.7f);
		bowAnimator.SetBool ("shoot", false);
        originalArrow.GetComponent<MeshRenderer>().enabled = true;
    }

	private void Shoot(){
		shoot = true;
        Vector3 fixedPosition = new Vector3(trOrigin.position.x, trOrigin.position.y, trOrigin.position.z);
        Instantiate(arrowGameObject, fixedPosition, new Quaternion(trOrigin.rotation.x, trOrigin.rotation.y, trOrigin.rotation.z, trOrigin.rotation.w));
    }
		
	private void LateUpdate(){
		//Animate if in right area and not shooting and not in the intro
		if (!shoot && !shouldRest) {
			bowAnimator.SetBool ("rightArea", bowOrientation.rightArea);
		}
        if (shouldRest)
        {
            bowAnimator.SetBool("rightArea", false);
        }

		if(bowAnimator.GetCurrentAnimatorStateInfo(0).IsTag("Shoot")){
			shoot = true;
        }else { 
			shoot = false;
		}
	}
}