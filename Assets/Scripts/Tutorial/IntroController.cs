﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class IntroController : MonoBehaviour
{
    [Header("--Step Objects--")]
    public GameObject[] stepArray;
    public GameObject lastStep;

    [Header("--Step Info.--")]
    public int totalSteps;
    public int currentStep;
    public int[] triggetType; //For each step --- 0- touch | 1- LookToTarget | 2- LookToTargetAndShoot | 3- time

    [Header("--Step Flow--")]
    public bool activatedTrigger;
    public bool changingStep;
    public bool isLastStep;
    public float timeLastStep;

    [Header("--Raycast Info.--")]
    public Camera camera;
    private bool looking;
    private float timeStartedToLook;
    public float timeAfterLook;
    public Image lookFeedbak;
    public Image[] lookTargetfeedbak;

    #region Show and Hide
    public void ShowStepImage(int step)
    {
        StartCoroutine(WaitToAble(step));
    }
    public void HideStepImage(int step)
    {
        StartCoroutine(WaitToUnable(step));
    }
    private IEnumerator WaitToAble(int step)
    {
        stepArray[step].SetActive(true);
        stepArray[step].GetComponent<Animator>().SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        stepArray[step].GetComponent<Animator>().SetBool("able", false);
        ResetLookingFeedbak();

    }
    public IEnumerator WaitToUnable(int step)
    {
        stepArray[step].GetComponent<Animator>().SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        stepArray[step].GetComponent<Animator>().SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        stepArray[step].SetActive(false);
    }
    public IEnumerator StartTransition()
    {
        ResetLookingFeedbak();

        //Start Changing
        changingStep = true;

        //Hide this
        HideStepImage(currentStep);
        lookTargetfeedbak[currentStep].gameObject.SetActive(false);


        //Wait
        yield return new WaitForSeconds(0.35f);

        //Current++ (check if is the last and if it is, end this)
        if (!IsLastStep())
        {
            NextStep();

            //Show next
            ShowStepImage(currentStep);

            //Wait
            yield return new WaitForSeconds(1.1f);

            //Finish Changing
            changingStep = false;
            activatedTrigger = false;
        }
        else
        {
            //send a message to the gameController to end the intro part
            gameObject.GetComponent<GameController>().EndIntro();
        }

    }
    public void ShowLastStep()
    {
        StartCoroutine(ShowLast());
    }
    private IEnumerator ShowLast()
    {
        if(AudioController.instance != null)
        {
            AudioController.instance.TransitionBgToQuiz();
        }
        
        isLastStep = true;
        changingStep = true;
        activatedTrigger = true;
        lastStep.SetActive(true);
        lastStep.GetComponent<Animator>().SetBool("able", true);
        yield return new WaitForSeconds(0.1f);
        lastStep.GetComponent<Animator>().SetBool("able", false);
        yield return new WaitForSeconds(4);
        gameObject.GetComponent<GameController>().EndPreGame();
        yield return new WaitForSeconds(timeLastStep);
        lastStep.GetComponent<Animator>().SetBool("unable", true);
        yield return new WaitForSeconds(0.1f);
        lastStep.GetComponent<Animator>().SetBool("unable", false);
        yield return new WaitForSeconds(0.8f);
        lastStep.SetActive(false);
        changingStep = false;
        activatedTrigger = false;

        
    }
    #endregion

    #region Step
    public int GetCurrentStep()
    {
        return currentStep;
    }
    public void NextStep()
    {
        currentStep++;
        //If is the last step... 
        if (currentStep >= totalSteps)
        {
            currentStep = totalSteps;

            //send a message to the gameController to end the intro part
            gameObject.GetComponent<GameController>().EndStep();
        }
    }
    public bool IsLastStep()
    {
        if ((currentStep + 1) == totalSteps)
        {
            return true;
        }
        return false;
    }
    #endregion

    #region Trigger
    public int GetTriggetType()
    {
        return triggetType[currentStep];
    }
    public void Update()
    {
        //If in the right Scene - Quiz scene
        if (gameObject.GetComponent<GameController>().currentScene == gameObject.GetComponent<GameController>().sceneName[1])
        {
            if (!activatedTrigger && !changingStep && !isLastStep)
            {
                switch (GetTriggetType())
                {
                    case 0: //0 - touch
                        if (Input.GetButtonDown("Fire1"))
                        {
                            activatedTrigger = true;
                            StartCoroutine(StartTransition());
                        }
                        break;

                    case 1: //1 - LookToTarget
                        if (LookingAtTarget("target") && !looking){
                            lookTargetfeedbak[currentStep].gameObject.SetActive(true);
                            looking = true;
                            timeStartedToLook = Time.time;
                            ResetLookingFeedbak();
                        } else if(!LookingAtTarget("target")){
                            lookTargetfeedbak[currentStep].gameObject.SetActive(false);
                            looking = false;
                            timeStartedToLook = 0;
                            ResetLookingFeedbak();
                        }
                        if (looking && timeStartedToLook != 0)
                        {
                            UpdateLookingFeedback();
                            if (timeAfterLook + timeStartedToLook <= Time.time)
                            {
                                lookTargetfeedbak[currentStep].gameObject.SetActive(false);
                                activatedTrigger = true;
                                looking = false;
                                timeStartedToLook = 0;
                                StartCoroutine(StartTransition());
                            }
                        }
                        break;

                    case 2: //2 - LookToTargetAndShoot
                        if (LookingAtTarget("target"))
                        {
                            lookTargetfeedbak[currentStep].gameObject.SetActive(true);
                            if (Input.GetButtonDown("Fire1"))
                            {
                                activatedTrigger = true;
                                StartCoroutine(StartTransition());
                            }
                        }
                        else
                        {
                            lookTargetfeedbak[currentStep].gameObject.SetActive(false);
                        }
                        break;

                    case 3: //3 - time
                        if (Input.GetButtonDown("Fire1"))
                        {
                            activatedTrigger = true;
                            StartCoroutine(StartTransition());
                        }
                        break;
                }
            }
        }
    }
    private void UpdateLookingFeedback()
    {
        float timeLeft = ((timeStartedToLook + timeAfterLook) - Time.time);
        float amount = ((timeLeft / timeAfterLook) - 1) *-1;
        lookFeedbak.fillAmount = amount;
    }
    private void ResetLookingFeedbak()
    {
        lookFeedbak.fillAmount = 0;
    }
    private bool LookingAtTarget(string obj)
    {
        //Raycast foward the camera
        Ray ray = camera.ViewportPointToRay(new Vector3(0.5F, 0.5F, 0));
        RaycastHit hit;

        //If the raycast hits something
        if (Physics.Raycast(ray, out hit))
        {
            if (hit.transform.name == obj)
            {
                return true;
            }
        }
        return false;
    }
    #endregion
}