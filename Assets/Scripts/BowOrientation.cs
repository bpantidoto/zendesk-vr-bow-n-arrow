﻿using UnityEngine;
using System.Collections;

public class BowOrientation : MonoBehaviour {

	private Transform tr;
	public Transform trCamera;
	private Vector3 initialAngle;
	public float speed;

	public float angleMinX1;
	public float angleMaxX1;
	public float angleMinX2;
	public float angleMaxX2;
	public float angleMinY1;
	public float angleMaxY1;
	public float angleMinY2;
	public float angleMaxY2;

	public bool rightArea;


	private void Start(){
		tr = gameObject.GetComponent<Transform>();
		initialAngle = tr.rotation.eulerAngles;
	}

	private void LateUpdate(){
		Move ();
	}

	//Makes the objects move only after a few seconds
	private void Move(){
		//Save the current position
		Quaternion currentRotation = tr.rotation;

		//Save the position of the camera
		Quaternion cameraRotation = trCamera.rotation;

		//Check what angles will be updated
		bool rotateX = (cameraRotation.eulerAngles.x >= angleMinX1 && cameraRotation.eulerAngles.x <= angleMaxX1 || cameraRotation.eulerAngles.x >= angleMinX2 && cameraRotation.eulerAngles.x <= angleMaxX2);
		bool rotateY = (cameraRotation.eulerAngles.y >= angleMinY1 && cameraRotation.eulerAngles.y <= angleMaxY1 || cameraRotation.eulerAngles.y >= angleMinY2 && cameraRotation.eulerAngles.y <= angleMaxY2);
		bool rotateXY = rotateX && rotateY;

		//Update the correct angles
		Quaternion rotateTo = new Quaternion();
		if (rotateXY) {
			//Rotate x and y (fixed Z)
			rotateTo = Quaternion.Euler (cameraRotation.eulerAngles.x, cameraRotation.eulerAngles.y, initialAngle.z);
			rightArea = true;
		} 
		else if (rotateX) {
			//Rotate x (fixed Z)
			rotateTo = Quaternion.Euler (cameraRotation.eulerAngles.x, currentRotation.eulerAngles.y, initialAngle.z);
			rightArea = false;
		}
		else if (rotateY) {
			//Rotate y (fixed Z)
			rotateTo = Quaternion.Euler (currentRotation.eulerAngles.x, cameraRotation.eulerAngles.y, initialAngle.z);
			rightArea = false;
		}
		else { rightArea = false; }
		tr.rotation = Quaternion.Lerp(currentRotation, rotateTo, speed);
			
		//Limits Stablelizer
		if(!rotateX){
			if(currentRotation.eulerAngles.x > angleMinX2){
				rotateTo = Quaternion.Euler (angleMinX2, currentRotation.eulerAngles.y, initialAngle.z);
			}
			if(currentRotation.eulerAngles.x < angleMaxX1){
				rotateTo = Quaternion.Euler (angleMaxX1, currentRotation.eulerAngles.y, initialAngle.z);
			}
		}
		if(!rotateY){
			if(currentRotation.eulerAngles.y > angleMinY2){
				rotateTo = Quaternion.Euler (currentRotation.eulerAngles.x, angleMinY2, initialAngle.z);
			}
			if(currentRotation.eulerAngles.y < angleMaxY1){
				rotateTo = Quaternion.Euler (currentRotation.eulerAngles.x, angleMaxY1, initialAngle.z);
			}
		}
	}
}